package zajecia3;

import java.util.Scanner;

public class StringLengthCount {
    public static void main(String[] args) {

        //utworzyc scanner
        Scanner sc = new Scanner(System.in);

        //zapytac o napis
        System.out.println("Wpisz dowolny tekst");

        //pobrac linie tekstu
        String line = sc.nextLine();

        //wyswietlic menu 1. z bialymi znakami 2. bez bialych znakow
        System.out.println("1.Policz dlugosc z bialymi znakami");
        System.out.println("2.Policz dlugosc bez bialych znakow");

        int wybor = sc.nextInt();
        //swich case
        switch (wybor) {
            case 1:

                System.out.println(line.length());
                break;

            case 2:
                int licznik = 0;
                for (int i = 0; i < line.length(); i++){
                    //jezeli znak na i-tej pozycji nie jest bialym znakiem to zliczaj
                    if (line.charAt(i) != ' ' && line.charAt(i) != '\t'){
                        licznik++;
                    }
                }
                System.out.println(licznik);
                break;
            default:
                System.out.println("Zly wybor");
                break;
        }



    }

}
