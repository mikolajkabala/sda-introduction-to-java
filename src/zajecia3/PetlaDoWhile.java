package zajecia3;

// od 0 - 100
// wypisac parzyste z zakresu 0 - 100 z uzyciem do while
public class PetlaDoWhile {

    public static void main(String[] args) {
        int licznik = 0;
        do {
            if(licznik % 2 != 0){
                System.out.println(licznik);
            }
            licznik++;
        } while (licznik < 100);

    }
}
