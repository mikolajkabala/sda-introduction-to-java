package zajecia5;
//klasa pomocnicza - zawiera metody do operacji na tablicach
public class ArrayHelper {
    public static boolean isPresent(int[] array, int element) {
        boolean present = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i]  == element) {
                present = true;
                break;
            }
        }
        return present;
     }

    /**
     * Metoda wyswietlajaca wszystkie elementy tablicy (dla tablicy int)
     * w jednej lini
     */
    public static void printArray(int[] array){
        for (int i = 0; i < array.length; i++){
            System.out.println(array[i] + " ");
        }
        System.out.println();
    }

}
