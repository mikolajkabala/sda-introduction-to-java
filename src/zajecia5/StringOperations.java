package zajecia5;

import java.util.Scanner;

public class StringOperations {
    public static void main(String[] args) {

        //Wczytaj​ ​ od​ ​ użytkownika​ ​ napis​ ​ o ​ ​ długości​ ​ do​ ​ 20​ ​ znaków
        // a ​ ​ nastepnie​ ​ wykonaj​ ​ na​ ​ nim poniższe​ ​ operacje

        Scanner sc = new Scanner(System.in);
        System.out.println("Wpisz napis o dlugosci do 20 znakow ");
        String text = sc.nextLine();

        if (text.length() < 20) {
            printMenu();
            int choice = sc.nextInt();
            switch (choice) {
                case 1:
                    toUpper(text);
                    break;
                case 2:
                    toLover(text);
                    break;
                case 3:
                    toggleCase(text);
            }
        } else {
            System.out.println("Napis zbyt dlugi");
        }

    }

    public static void printMenu() {
        System.out.println("1. Wszystkie litery na duze");
        System.out.println("2. Wszystkie litery na male");
        System.out.println("3. Wszystkie male na duze i duze na male");
        System.out.println("Twoj wybor: ");
    }

    public static void toUpper(String input) {

        String result = "";
        for (int i = 0; i < input.length(); i++) {
            char element = input.charAt(i);

            if (element >= 97 && element <= 122) {
                element -= 32;
            }
            result += element;
        }
        System.out.println(result);
    }


    public static void toLover(String str) {
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] >= 65 && chars[i] <= 90) {
                chars[i] += 32;
            }
        }
        String result = new String(chars);
        System.out.println(result);
    }

    public static void toggleCase(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char element = str.charAt(i);
            if (element >= 65 && element <= 90) {
                element += 32;
            } else if (element >= 97 && element <= 122){
                element -= 32;
            }
            sb.append(element);

        }
        String result = sb.toString();
        System.out.println(result);


    }
}