package zajecia5;

import java.util.Random;
import java.util.Scanner;

public class SearchInArray {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //utworzyc tablice 30 elem
        int[] tablica = new int[30];

        //wypelnic losowymi wartosciami
        Random rd = new Random();
        for (int i = 0; i< tablica.length; i++){
            int pomocnicza = rd.nextInt(100);
            tablica[i] = pomocnicza;
        }
        //uzytkownik wprowadza jakas liczbe
        System.out.println("Podaj liczbe od 0 do 100. Sprawdze czy taka wystepuje ...");
        int liczba = sc.nextInt();

        //sprawdzamy czy taka liczba wystepuje w tablice
        boolean czyObecna = false;
        for (int i = 0; i < tablica.length; i++){
            if (tablica[i] == liczba){
                czyObecna = true;
                break;
            }
        }

        boolean wynik = ArrayHelper.isPresent(tablica, liczba);
            if (wynik){
            System.out.println("Element wystepuje w tablicy");
        }else {
            System.out.println("Element nie wystepuje w tablicy");
        }

    }
}
