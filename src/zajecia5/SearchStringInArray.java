package zajecia5;

import java.util.Scanner;

public class SearchStringInArray {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //utworzyc tablice 5 napisow
        //i od razu je zainicjalizowac

 //       String[] nazwiska = new  String[5];
 //       nazwiska[0] = "Kowalski";
 //       nazwiska[1] = "Nowak";
 //       nazwiska[2] = "Adamiak";
 //       nazwiska[3] = "Nowacki";
 //       nazwiska[4] = "Kowal";

        //utworzenie tablicy 5 elementowej z inicjalizacja
        //rozmiar tablicy jest wywnioskowany po liczbie elementow podanych w nazwiskach
        String[] surnames = {"Kowalski", "Nowak", "Adamiak", "Nowacki", "Kowal"};


        //zapytac uzytkownika o nazwisko

        System.out.println("Podaj nazwisko: ");
        String nazwisko = sc.nextLine();
        boolean isPresent = false;
        for (int i = 0; i < surnames.length; i++){
            if (surnames[i].equals(nazwisko)){
                isPresent = true;
                break;
            }
        }
        System.out.println(isPresent);

        //odpowiedziec czy takie nazwisko wystepuje
    }
}
