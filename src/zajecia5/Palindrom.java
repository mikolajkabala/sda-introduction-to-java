package zajecia5;

import java.util.Scanner;

/*
Wczytaj​ ​ od​ ​ użytkownika​ ​ napis​ ​ o ​ ​ długości​ ​ do​ ​ 30​ ​ znaków,​ ​ a ​ ​ następnie​ ​ wyświetl​ ​ go​ ​ od​ ​ końca.
Sprawdź​ ​ czy​ ​ podany​ ​ napis​ ​ jest​ ​ palindromem​ ​ oraz​ ​ określ​ ​ jego​ ​ parzystość/nieparzystość.
Przy​ ​ sprawdzaniu​ ​ należy​ ​ ignorować​ ​ znaki​ ​ białe.
 */
public class Palindrom {

    public static boolean isPalindrome(String text){
        text = text.replace("","").toLowerCase();
        for (int i = 0; i < text.length() / 2; i++){
            if (text.charAt(i) != text.charAt(text.length() - 1 - i)){
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Wpisz napis o dlugosci do 30 znakow - sprawdze czy to palindrom");
        String text = sc.nextLine();
        System.out.println("Napis :" + text + ", wynik: " + isPalindrome(text));
    }
}
