package zajecia4;

import java.util.Scanner;

public class TriangleExcercise {
    public static double obliczPole(double a, double b, double c) {
        double p = (a + b + c) / 2;
        double pole = Math.sqrt(p * (p - a) * (p - b) * (p - c));
        return pole;
    }
    //Metoda obliczajaca pole trojkata



    public static void main(String[] args) {
        //todo: wczytac 3 dlugosci typy double

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj dlugosc boku trojkata: a");
        double a = sc.nextDouble();

        System.out.println("Podaj dlugosc boku trojkata: b");
        double b = sc.nextDouble();

        System.out.println("Podaj dlugosc boku trojkata: c");
        double c = sc.nextDouble();

        //sprawdzic czy mozna zbudowac trojkat
        // a + b > c
        // a + c > b
        // b + c > a

        if ((a + b > c) && (a + c > b) && (b + c > a)) {
            System.out.println("Mozna zbudowac trojkat");

            //jezeli tak -> policz pole
            //  double p = 0.5  * (a + b + c);
            //double pole = Math.sqrt(p * (p - a) * (p - b) * (p - c));

            double pole = obliczPole(a, b, c);
            System.out.println("Pole trojkata wynosi: " + pole);

            //else -> wyswietlic komunikat
        } else {
            System.out.println("Nie mozna zbudowac trojkata");
        }

}







    }

