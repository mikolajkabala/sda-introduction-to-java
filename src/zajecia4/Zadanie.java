package zajecia4;

/*
Napisz program wczytujący ciąg liczb rzeczywistych. Wydrukuj na ekranie kolejno
wszystkie liczby, które należą do przedziału [4;15).
 */

import java.util.Scanner;

public class Zadanie {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj rozmiar tablicy: ");
        int rozmiar = scanner.nextInt();
        int[] tablica = new int[rozmiar];

        //wypisywanie wartosci przez uzytkownika
        for (int i = 0; i < tablica.length; i++) {
            System.out.println("Podaj " +(i + 1) + " wartosc: ");
            tablica[i] = scanner.nextInt();
        }
        //wyswietlanie wartoscia
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] >= 4 && tablica[i] < 15) {
 //               System.out.println(tablica[i] + " ");
                System.out.println("Indeks: " + i + " wartosc " + tablica[i]);
            }
        }


    }
}
