package zajecia4;

import java.util.Random;

//wylosuj 10 wartosci i znajdz najwieksza
public class FindMaxInArray {
    public static void main(String[] args) {
        Random rd = new Random();
        int[] array = new int[10];

        //wypisywanie wartosci do tablicy
        for (int i = 0; i < array.length; i++) {
            array[i] = rd.nextInt(50);
        }

        //wypisz wartosci
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
     //szukanie max
     //zakladamy wstepnie, ze najwieksza wartosc jest w tablicy o indeksie )
     int max = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }
        System.out.println("Najwieksza wartosc to: " + max);
    }
}
