package zajecia2.instrukcjesterujace;

import java.util.Scanner;

public class IfStatment {
    public static void main(String[] args) {

        int wiek;

        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj wiek: ");
        wiek = odczyt.nextInt();

        if (wiek >= 18) {
            System.out.println("Jestes pelnoletni");
        }
        else {
            System.out.println("Nie jestes pelnoletni");
        }
        System.out.println("Witaj w systemie");
    }
}
