package zajecia2.instrukcjesterujace;
/*
    Dla podanych dwoch liczb z klawiatury wypisuje wieksze z nich
    Zakladamy, ze nie podajemy dwoch takich samych
    A != B
 */

import java.util.Scanner;

public class FindGreatetNumber {
    public static void main(String[] args) {

        int a;
        int b;

        System.out.println("Podaj liczby calkowite,  a != b: ");

        Scanner odczyt = new Scanner(System.in);
        a = odczyt.nextInt();
        b = odczyt.nextInt();

        if (a > b){
            System.out.println("Wieksza jest liczba a: " + a);
    } else if (a < b){
            System.out.println("Wieksza jest liczba b: " + b);
        } else {
            System.out.println("Liczby sa rowne");
        }

}
    }
