package zajecia2.instrukcjesterujace;

import java.util.Scanner;

public class BMI {
    public static void main(String[] args) {
        double waga;
        double wzrost;

        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj swoj wzrost w metrach: ");
        wzrost = odczyt.nextDouble();

        System.out.println("Podaj swoja wage: ");
        waga = odczyt.nextDouble();

        //double bmi = waga / (wzrost * wzrost);
        double bmi = waga / (float)Math.pow(wzrost, 2);

        System.out.println("Twoje BMI: " + bmi);

        if (bmi < 18.5) {
            System.out.println("Masz niedowage");
        } else if(bmi >= 18.5 && bmi < 25){
            System.out.println("Waga ok");
        } else if (bmi >= 25 && bmi < 29){
            System.out.println("Nadwaga");
        } else {
            System.out.println("Otylosc I stopnia");
        }

}
}
