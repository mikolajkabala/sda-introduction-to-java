package konwerter;

import java.util.Scanner;

public class Konwerter1 {
    public static void main(String[] args) {
        double tempCel;

        Scanner odczyt = new Scanner(System.in);
        System.out.print("Podaj temperature w stopniach Celsjusza: ");
        tempCel = odczyt.nextDouble();

        double wynik = tempCel * 1.8 + 32;

        System.out.println("Przeliczenie temperatury z " + tempCel + " na F " + wynik);



    }
}
