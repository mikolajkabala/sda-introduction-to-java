package companyManager.fileoperation.reader;

import companyManager.Employee;

/**
 * Abstrakcyjny czytnik - jedyne co mozmey o nim powiedziec, to ze bedziemy czytac z pliku
 * Wiec tutaj przechowujemy sceizke do pliku
 */
public abstract class AbstractEmployeeReader implements EmployeeReader {

    protected String pathToFile;

    protected AbstractEmployeeReader(String pathToFile) {
        this.pathToFile = pathToFile;
    }

    @Override
    public abstract Employee[] readEmployees();
}
