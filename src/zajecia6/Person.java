package zajecia6;

/**
 * Klasa Person do reprezentacji danych o osobie
 */
public class Person {
    //stan  - mowi o tym jaka ta osoba jest - przechowuje jej dane
    public String name;
    public String surname;
    public int age;
    public String email;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    //zachowanie - jakas czynnosc
    public void sayHello(){
        System.out.println("Jestem: " + name + " " + surname);
    }
}
