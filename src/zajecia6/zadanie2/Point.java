package zajecia6.zadanie2;

/**
 * Zdefiniuj​ ​ klasę​ ​ Point​ ​ do​ ​ reprezentacji​ ​ Punktu​ ​ w ​ ​ przestrzeni​ ​ kartezjańskiej.​ ​ Klasa​ ​ powinna
 zawierać​ ​ pola​ ​ x,​ ​ y ​ ​ oraz​ ​ metodę​ ​ distanceFromOrigin(),​ ​ która​ ​ będzie​ ​ zwracać​ ​ odległość​ ​ od
 początku​ ​ układu​ ​ współrzędnych.
 */
public class Point {
    private int x;
    private int y;

    //konstruktor bezparametrowy
//    public Point(){
//        System.out.println("bezparametrowy");
//    }

    //przeciazony konstruktor
    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX(){
        return x;
    }
    public void setX(int x){
        this.x = x;
    }

    public int getY(){
        return y;
    }
    public void setY(int y){
        this.y = y;
    }
    //obliczajaca odleglosc od srodka
    public double distance(){
        return Math.sqrt(x * x + y * y);
    }


}
