package zajecia6.zadanie4;

/**
 * Zdefiniuj​ ​ klasę​ ​ Pracownik.​ ​ Następnie​ ​ zdefiniuj​ ​ klasę​ ​ Firma,​ ​ która​ ​ by​ ​ przechowywała​ ​ w
 tablicy​ ​ spis​ ​ wszystkich​ ​ pracowników​ ​ (możesz​ ​ założyć,​ ​ że​ ​ liczba​ ​ pracowników​ ​ nie​ ​ przekracza
 100).​ ​ Zdefiniuj​ ​ metody​ ​ dodawania​ ​ nowych​ ​ pracowników​ ​ do​ ​ firmy​ ​ oraz​ ​ wypisywania
 aktualnego​ ​ spisu​ ​ pracowników.
 */
public class Employee {
    private String name;
    private String surname;
    private int age;
    private String email;
    private double salary;


    public String getName() {
        return name;
    }

    public void setName(String name){
        this.name = name;
    }
    public String getSurname(){
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
    public int getAge(){
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
    public String getDescription() {
        return String.format("Name: %s, Surname: %s, Salary: %f", name, surname, salary);

}

}

